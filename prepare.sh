#!/bin/bash

[[ $EUID -ne 0 ]] && echo "This script must be run as root." && exit 1

cat > /etc/apt/apt.conf.d/999norecommends << EOF
APT::Install-Recommends "0";
APT::Install-Suggests "0";
EOF

apt update && apt install --install-recommends -y git 
apt install -y ansible
ansible-pull -U https://codeberg.org/gareth/my-ansible-setup.git